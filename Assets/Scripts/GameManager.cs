﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static bool GameIsOver;

    public GameObject gameOverUI;

    public Transform endPont;

    void Start()
    {
        GameIsOver = false;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (GameIsOver)
            return;

        if (collision.gameObject.tag == "Enemy")
        {
            EndGame();
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    void EndGame()
    {
        GameIsOver = true;
        gameOverUI.SetActive(true);
    }

    void Retry()
    {
        SceneManager.LoadScene("MainScene");
    }
}

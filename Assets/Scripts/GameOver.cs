﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    public GameObject gameOverUI;

    public static GameOver instance;
    private void Start()
    {
        instance = this;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Enemy")
        {
            Debug.Log("d");
            gameOverUI.SetActive(true);
        }
    }
    public void Retry()
    {
        SceneManager.LoadScene("MainScene");
    }
}
